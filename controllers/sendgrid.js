const dotenv = require("dotenv");
const sgMail = require("@sendgrid/mail");

dotenv.config();
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

exports.sendMail = (req, res, next) => {
  sgMail.send(req.msg, (error, result) => {
    if (error) {
      res.status(400).send({
        message: "Server Error"
      });
    } else {
      res.status(200).send({
        message: "Link sent to mail"
      });
    }
  });
};
