const crypto = require("crypto");
const uuidv1 = require("uuid/v1");

const jwt = require("jsonwebtoken");

const Admin = require("../models").admin;
const ActiveBid = require("../models").ActiveBid;
const CompletedBid = require("../models").CompletedBid;

exports.login = (req, res, next) => {
  Admin.findOne({ where: { username: req.body.username } }).then(function(
    Admin
  ) {
    if (Admin) {
      new_hashed_pwd = crypto
        .createHash("sha1", Admin.salt)
        .update(req.body.password)
        .digest("hex");
      if (new_hashed_pwd === Admin.password) {
        const token = jwt.sign(
          { username: Admin.username },
          process.env.JWT_ENCRYPTION,
          {
            expiresIn: "1h"
          }
        );
        return res
          .status(200)
          .send({ message: "Login Successfull", token: token });
      } else {
        return res.status(400).send({ message: "Wrong credentials" });
      }
    } else {
      return res.status(401).send({ message: "Wrong credentials" });
    }
  });
};

exports.register = (req, res, next) => {
  new_salt = uuidv1();
  new_hashed_pwd = crypto
    .createHash("sha1", new_salt)
    .update(req.body.password)
    .digest("hex");
  Admin.findOne({ where: { username: req.body.username } }).then(function(
    check_admin
  ) {
    if (check_admin) {
      return res.status(401).send({ message: "Admin already registered" });
    } else {
      Admin.create({
        username: req.body.username,
        password: new_hashed_pwd,
        salt: new_salt
      }).then(function(Admin) {
        if (Admin) {
          const token = jwt.sign(
            { username: Admin.username },
            process.env.JWT_ENCRYPTION,
            {
              expiresIn: "1h"
            }
          );
          return res.status(200).send({
            message: "Admin registered",
            token: token
          });
        } else {
          return res.status(400).send({ message: "Server error" });
        }
      });
    }
  });
};

exports.addProducts = (req, res, next) => {
  const products = req.body.products;
  for (var i = 0; i < products.length; i++) {
    const name = products[i]["product_name"];
    const initial_bid = products[i]["initial_bid"];
    ActiveBid.findOne({
      where: { product_name: name }
    })
      .then(function(products) {
        if (products) {
          products.update({
            initial_bid: initial_bid,
            current_bid: initial_bid
          });
        } else {
          ActiveBid.create({
            product_name: name,
            initial_bid: initial_bid,
            current_bid: initial_bid,
            buyer: "",
            active: false
          });
        }
      })
      .catch(() => {
        return res.status(400).send({ message: "server error" });
      });
  }
  return res.status(200).send({ message: "Products added" });
};

exports.updateProduct = (req, res, next) => {
  const products = req.body;
  const name = products["product_name"];
  const initial_bid = products["initial_bid"];
  const active = products["active"];
  ActiveBid.findOne({
    where: { product_name: name }
  })
    .then(function(products) {
      if (products) {
        products
          .update({
            initial_bid: initial_bid,
            current_bid: initial_bid,
            active: active
          })
          .then(() => {
            setTimeout(() => {
              console.log("...Bidding for this product completed...");
              ActiveBid.findOne({
                where: { product_name: name }
              }).then(product => {
                CompletedBid.create({
                  id: product.id,
                  product_name: product.product_name,
                  initial_bid: initial_bid,
                  final_bid: product.current_bid,
                  buyer: product.buyer,
                  active: false
                });
                ActiveBid.destroy({ where: { product_name: name } });
              });
            }, 50 * 1000);
            return res.status(200).send({ message: "Products updated" });
          });
      } else {
        return res.status(401).send({ message: "product not found" });
      }
    })
    .catch(() => {
      return res.status(400).send({ message: "server error" });
    });
};

exports.deleteProduct = (req, res, next) => {
  const products = req.body;
  const name = products["product_name"];
  ActiveBid.findOne({
    where: { product_name: name }
  })
    .then(function(products) {
      if (products) {
        ActiveBid.destroy({
          where: { product_name: name }
        }).then(function() {
          return res.status(200).send({ message: "Product deleted" });
        });
      } else {
        return res.status(401).send({ message: "product not found" });
      }
    })
    .catch(() => {
      return res.status(400).send({ message: "server error" });
    });
};

exports.listAllProducts = (req, res, next) => {
  ActiveBid.findAll({
    attributes: ["id", "product_name", "active", "current_bid"]
  })
    .then(products => {
      let list = [];
      for (var i = 0; i < products.length; i++) {
        list.push(products[i].dataValues);
      }
      return res.status(200).send({ products: list });
    })
    .catch(() => {
      return res.status(400).send({ message: "server error" });
    });
};
