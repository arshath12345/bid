const crypto = require("crypto");
const uuidv1 = require("uuid/v1");
const { Op } = require("sequelize");
const jwt = require("jsonwebtoken");

const User = require("../models").user;
const ActiveBid = require("../models").ActiveBid;
const CompletedBid = require("../models").CompletedBid;
const History = require("../models").History;

exports.getTwoFactor = (req, res, next) => {
  User.update({ two_factor: true }, { where: { username: req.user.username } })
    .then(() => {
      return res.status(200).send({ message: "Two factor auth is enabled" });
    })
    .catch(() => {
      return res.status(400).send({ message: "Server Error" });
    });
};

exports.removeTwoFactor = (req, res, next) => {
  User.update({ two_factor: false }, { where: { username: req.user.username } })
    .then(() => {
      return res.status(200).send({ message: "Two factor auth is disabled" });
    })
    .catch(() => {
      return res.status(400).send({ message: "Server Error" });
    });
};

exports.twofactorLogin = (req, res, next) => {
  const token = jwt.sign({ email: req.email }, process.env.JWT_ENCRYPTION, {
    expiresIn: "1h"
  });
  return res.status(200).send({ message: "Login Successfull", token: token });
};

exports.login = (req, res, next) => {
  if (!req.body.username) {
    req.body.username = "";
  }
  if (!req.body.email) {
    req.body.email = "";
  }
  User.findOne({
    where: {
      [Op.or]: [{ username: req.body.username }, { email: req.body.email }]
    }
  }).then(function(User) {
    if (User) {
      new_hashed_pwd = crypto
        .createHash("sha1", User.salt)
        .update(req.body.password)
        .digest("hex");
      if (new_hashed_pwd === User.password) {
        if (User.dataValues.two_factor === true) {
          req.user = User.dataValues;
          next();
        } else {
          const token = jwt.sign(
            { username: req.body.username, email: req.body.email },
            process.env.JWT_ENCRYPTION,
            {
              expiresIn: "1h"
            }
          );
          return res
            .status(200)
            .send({ message: "Login Successfull", token: token });
        }
      } else {
        return res.status(400).send({ message: "Wrong credentials" });
      }
    } else {
      return res.status(401).send({ message: "Wrong credentials" });
    }
  });
};

exports.register = (req, res, next) => {
  new_salt = uuidv1();
  new_hashed_pwd = crypto
    .createHash("sha1", new_salt)
    .update(req.body.password)
    .digest("hex");
  User.findOne({
    where: {
      [Op.or]: [{ username: req.body.username }, { email: req.body.email }]
    }
  }).then(function(check_user) {
    if (check_user) {
      return res.status(401).send({ message: "User already registered" });
    } else {
      User.create({
        username: req.body.username,
        email: req.body.email,
        password: new_hashed_pwd,
        verified: false,
        salt: new_salt
      }).then(function(user) {
        if (user) {
          const token = jwt.sign(
            { username: user.username, email: user.email },
            process.env.JWT_ENCRYPTION,
            {
              expiresIn: "1h"
            }
          );
          return res.status(200).send({
            message: "User registered",
            token: token
          });
        } else {
          return res.status(400).send({ message: "Server error" });
        }
      });
    }
  });
};

exports.getActiveProducts = (req, res, next) => {
  ActiveBid.findAll({ where: { active: true } })
    .then(products => {
      let list = [];
      for (var i = 0; i < products.length; i++) {
        list.push(products[i].dataValues);
      }
      return res.status(200).send({ products: list });
    })
    .catch(err => {
      return res.status(400).send({ message: "Server error" });
    });
};

exports.bid = (req, res, next) => {
  const product_name = req.body.product_name;
  ActiveBid.findOne({
    where: { product_name: product_name }
  }).then(product => {
    if (product) {
      if (product.dataValues.active === true) {
        const buyer = req.user.username;
        const current_bid = req.body.amount;
        if (product.dataValues.current_bid < current_bid) {
          product
            .update({ buyer: buyer, current_bid: current_bid })
            .then(function() {
              History.findOne({ where: { id: product.dataValues.id } }).then(
                function(history) {
                  if (history) {
                    history
                      .update({
                        prev_user1: buyer,
                        prev_amount1: current_bid,
                        prev_user5: history.dataValues.prev_user4,
                        prev_amount5: history.dataValues.prev_amount4,
                        prev_user4: history.dataValues.prev_user3,
                        prev_amount4: history.dataValues.prev_amount3,
                        prev_user3: history.dataValues.prev_user2,
                        prev_amount3: history.dataValues.prev_amount2,
                        prev_user2: history.dataValues.prev_user1,
                        prev_amount2: history.dataValues.prev_amount1
                      })
                      .then(() => {
                        return res
                          .status(200)
                          .send({ message: "Bid successfull" });
                      });
                  } else {
                    History.create({
                      id: product.dataValues.id,
                      product_name: product.dataValues.product_name,
                      prev_user1: buyer,
                      prev_amount1: current_bid,
                      prev_user5: "",
                      prev_amount5: 0,
                      prev_user4: "",
                      prev_amount4: 0,
                      prev_user3: "",
                      prev_amount3: 0,
                      prev_user2: "",
                      prev_amount2: 0
                    }).then(() => {
                      return res
                        .status(200)
                        .send({ message: "Bid successfully done" });
                    });
                  }
                }
              );
            });
        } else {
          res.status(401).send({ message: "The bid amount is lesser" });
        }
      } else {
        res
          .status(401)
          .send({ message: "Bid not available for the product now" });
      }
    } else {
      res
        .status(401)
        .send({ message: "Bid not available for the product now" });
    }
  });
};

exports.boughtProducts = (req, res, next) => {
  CompletedBid.findAll({
    where: { buyer: req.user.username }
  })
    .then(products => {
      if (products) {
        const bought = [];
        for (var i = 0; i < products.length; i++) {
          bought.push({
            name: products[i].dataValues.product_name,
            id: products[i].dataValues.id,
            initial_amount: products[i].dataValues.initial_bid,
            final_amount: products[i].dataValues.final_bid
          });
        }
        return res.status(200).send({ products: bought });
      } else {
        return res.status(401).send({ message: "No products found" });
      }
    })
    .catch(err => {
      return res.status(400).send({ message: "Server error" });
    });
};

exports.getHistory = (req, res, next) => {
  History.findAll()
    .then(products => {
      if (products) {
        const history = [];
        for (var i = 0; i < products.length; i++) {
          history.push({
            id: products[i].dataValues.id,
            product_name: products[i].dataValues.product_name,
            user1: products[i].dataValues.prev_user1,
            amount1: products[i].dataValues.prev_amount1,
            user2: products[i].dataValues.prev_user2,
            amount2: products[i].dataValues.prev_amount2,
            user3: products[i].dataValues.prev_user3,
            amount3: products[i].dataValues.prev_amount3,
            user4: products[i].dataValues.prev_user4,
            amount4: products[i].dataValues.prev_amount4,
            user5: products[i].dataValues.prev_user5,
            amount5: products[i].dataValues.prev_amount5
          });
        }
        return res.status(200).send({ products: history });
      } else {
        return res.status(401).send({ message: "No products found" });
      }
    })
    .catch(err => {
      return res.status(400).send({ message: "Server error" });
    });
};
