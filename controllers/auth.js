const jwt = require("jsonwebtoken");
const fs = require("fs");
const { Op } = require("sequelize");
const crypto = require("crypto");
const uuidv1 = require("uuid/v1");
const QuickEncrypt = require("quick-encrypt");
const dotenv = require("dotenv");
dotenv.config();

const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const User = require("../models").user;
const Admin = require("../models").admin;
const environment = require("./environment_chooser").environment;

const publicKey = fs.readFileSync(
  __dirname + process.env.PUBLIC_KEY_DIR,
  "utf-8"
);

const privateKey = fs.readFileSync(
  __dirname + process.env.PRIVATE_KEY_DIR,
  "utf-8"
);

exports.generateOTPSendMail = (req, res, next) => {
  otp = uuidv1();
  User.update(
    { OTP: otp },
    {
      where: {
        [Op.or]: [{ username: req.user.username }, { email: req.user.email }]
      }
    }
  ).then(() => {
    setTimeout(() => {
      User.update(
        { OTP: null },
        {
          where: {
            [Op.or]: [
              { username: req.user.username },
              { email: req.user.email }
            ]
          }
        }
      ).then(() => {
        console.log("OTP expired");
      });
    }, 50 * 1000); //making OTP invalid after 50 secs
  });
  new_salt = uuidv1();
  const link =
    environment[process.env.ENVI].url +
    "/user/userLogin2FAPage?u=" +
    QuickEncrypt.encrypt(req.user.email, publicKey) +
    "&id=" +
    QuickEncrypt.encrypt(new_salt, publicKey);

  User.update(
    {
      two_factor_salt: new_salt // to check during otp page
    },
    { where: { email: req.user.email } }
  ).then(function() {
    req.msg = {
      to: req.user.email,
      from: process.env.mail,
      subject: "OTP for two factor authentication",
      html:
        "Use the below OTP (Valid for only 50 secs) " +
        "<br>" +
        otp +
        "<br>in the below link<br>" +
        link
    };
    setTimeout(() => {
      User.update(
        { two_factor_salt: null },
        {
          where: {
            [Op.or]: [
              { username: req.user.username },
              { email: req.user.email }
            ]
          }
        }
      ).then(() => {
        console.log("OTP expired");
      });
    }, 50 * 1000); //making the salt invalid after 50 secs
    next(); //Sending Mail
  });
};

exports.verifyTwoFactorLink = (req, res, next) => {
  const email = QuickEncrypt.decrypt(req.query.u, privateKey);
  console.log(email);
  User.findOne({
    where: { email: email }
  })
    .then(function(User) {
      if (User) {
        if (
          QuickEncrypt.decrypt(req.query.id, privateKey) ===
          User.two_factor_salt
        ) {
          const link = environment[process.env.ENVI].url + "/user/userLogin2FA";
          const str =
            "<html><body><form action =" +
            link +
            " method=\"POST\"><input type='text' name = 'otp'/><input type='hidden' name = 'email' value = " +
            req.query.u +
            "></input><input type = 'submit'/></form></body></html>";
          res.send(str);
        } else {
          return res.status(401).send({ message: "Bad request" });
        }
      } else {
        return res.status(401).send({ message: "User not found" });
      }
    })
    .catch(err => {
      return res.status(400).send({ message: "Link expired" });
    });
};

exports.verifyTwoFactor = (req, res, next) => {
  const email = QuickEncrypt.decrypt(req.body.email, privateKey);
  User.findOne({ where: { email: email } }).then(user => {
    if (user) {
      if (user.dataValues.OTP === req.body.otp) {
        user.update({ OTP: null, two_factor_salt: null }); //making OTP null, since it's a OTP :)
        req.email = email;
        next();
      } else {
        user.update({ two_factor_salt: null });
        return res.status(401).send({ message: "Invalid OTP / OTP expired" });
      }
    } else {
      return res.status(400).send({ message: "Server error" });
    }
  });
};

exports.checkAuthUser = (req, res, next) => {
  var token = req.headers["authorization"];
  console.log(token);
  if (!token)
    return res.status(403).send({ auth: false, message: "No token provided." });
  jwt.verify(token, process.env.JWT_ENCRYPTION, (err, decoded) => {
    if (err) {
      return res
        .status(500)
        .send({ auth: false, message: "Failed to authenticate token." });
    } else {
      console.log(decoded.username, decoded.email);
      if (!decoded.username && !decoded.email) {
        return res.status(403).send({ auth: false, message: "Bad Request" });
      }
      if (!decoded.username) {
        decoded.username = "";
      }
      if (!decoded.email) {
        decoded.email = "";
      }
      User.findOne({
        where: {
          [Op.or]: [{ username: decoded.username }, { email: decoded.email }]
        }
      }).then(function(user) {
        if (user) {
          req.user = user.dataValues;
          next();
        } else {
          return res.status(403).send({ auth: false, message: "Bad Request" });
        }
      });
    }
  });
};

exports.checkAuthAdmin = (req, res, next) => {
  var token = req.headers["authorization"];
  if (!token)
    return res.status(403).send({ auth: false, message: "No token provided." });
  jwt.verify(token, process.env.JWT_ENCRYPTION, (err, decoded) => {
    if (err) {
      return res
        .status(500)
        .send({ auth: false, message: "Failed to authenticate token." });
    } else {
      console.log(decoded.username);
      if (!decoded.username) {
        return res.status(403).send({ auth: false, message: "Bad Request" });
      }
      Admin.findOne({
        where: {
          username: decoded.username
        }
      }).then(function(user) {
        if (user) {
          next();
        } else {
          return res.status(403).send({ auth: false, message: "Bad Request" });
        }
      });
    }
  });
};

exports.forgotPassword = (req, res, next) => {
  //get email id from request
  User.findOne({ where: { email: req.body.email } }).then(User => {
    if (User) {
      const curUser = User.dataValues;
      if (curUser.password === null) {
        //google signed in user - reset password is not possible
        return res
          .status(401)
          .send({ message: "Incorrect Email Id or Email Id doesnot exists" });
      } else {
        //generating link to send a mail.
        new_salt = uuidv1();
        const link =
          environment[process.env.ENVI].url +
          "/user/fogotPwdLink?u=" +
          QuickEncrypt.encrypt(User.email, publicKey) +
          "&id=" +
          QuickEncrypt.encrypt(new_salt, publicKey);

        User.update({
          forgotPasswordSalt: new_salt // to check during password reset
        }).then(function() {
          req.msg = {
            to: curUser.email,
            from: process.env.mail,
            subject: "Reset Password for the Bidding app",
            html: "Use the below link" + "<br>" + link
          };
          setTimeout(() => {
            User.update(
              { forgotPasswordSalt: null },
              {
                where: { email: req.body.email }
              }
            ).then(() => {
              console.log("Link expired");
            });
          }, 50 * 1000);
          next(); //Sending Mail
        });
      }
    } else {
      return res
        .status(400)
        .send({ message: "Incorrect Email Id/Email Id doesnot exists" });
    }
  });
};

exports.forgotPasswordLink = (req, res, next) => {
  const email = QuickEncrypt.decrypt(req.query.u, privateKey);
  User.findOne({
    where: { email: email }
  })
    .then(function(User) {
      if (User) {
        if (
          QuickEncrypt.decrypt(req.query.id, privateKey) ===
          User.forgotPasswordSalt
        ) {
          const link = environment[process.env.ENVI].url + "/user/resetPwd";
          res.send(
            "<html><body><form action =" +
              link +
              " method=\"POST\"><input type='text' name = 'pwd'/><input type='hidden' name = 'id' value = " +
              req.query.id +
              "></input><input type='hidden' name = 'email' value = " +
              req.query.u +
              "></input><input type = 'submit'/></form></body></html>"
          );
        } else {
          return res.status(401).send({ message: "Bad request" });
        }
      } else {
        return res.status(401).send({ message: "User not found" });
      }
    })
    .catch(err => {
      return res.status(400).send({ message: "Link expired" });
    });
};

exports.resetPwd = (req, res) => {
  const email = QuickEncrypt.decrypt(req.body.email, privateKey);
  const forgotPasswordSalt = QuickEncrypt.decrypt(req.body.id, privateKey);
  User.findOne({ where: { email: email } }).then(function(User) {
    if (User) {
      if (forgotPasswordSalt === User.forgotPasswordSalt) {
        new_salt = uuidv1();
        new_hashed_pwd = crypto
          .createHash("sha1", User.salt)
          .update(req.body.pwd)
          .digest("hex");
        User.update({
          forgotPasswordSalt: null, //link is active for one time updation only
          password: new_hashed_pwd,
          salt: new_salt
        }).then(function() {
          return res.status(200).send({ message: "Password updated" });
        });
      } else {
        return res.status(401).send({ message: "Bad Request" });
      }
    } else {
      return res.status(402).send({ message: "User not found" });
    }
  });
};

exports.verifyUser = (req, res, next) => {
  User.findOne({ where: { email: req.user.email } }).then(User => {
    if (User) {
      const curUser = User.dataValues;
      if (curUser.password === null) {
        //google signed in user - already verified
        return res.status(200).send({ message: "Verified" });
      } else {
        //generating link to send a mail.
        new_salt = uuidv1();
        const link =
          environment[process.env.ENVI].url +
          "/user/verifyUserLink?u=" +
          QuickEncrypt.encrypt(req.user.email, publicKey) +
          "&id=" +
          QuickEncrypt.encrypt(new_salt, publicKey);
        User.update({
          verifyUserSalt: new_salt // to check during verification
        }).then(function() {
          req.msg = {
            to: curUser.email,
            from: process.env.mail,
            subject:
              "Click the below link for verification (Link active for only 50 secs)",
            html: "Use the below link" + "<br>" + link
          };
          setTimeout(() => {
            User.update(
              { verifyUserSalt: null },
              {
                where: { email: req.user.email }
              }
            ).then(() => {
              console.log("Link expired");
            });
          }, 50 * 1000); //making the salt null after 50 secs
          next(); //Sending Mail
        });
      }
    } else {
      return res
        .status(401)
        .send({ message: "Incorrect Email Id/Email Id doesnot exists" });
    }
  });
};

exports.verifyUserLink = (req, res, next) => {
  const email = QuickEncrypt.decrypt(req.query.u, privateKey);
  if (!email) {
    return res.status(400).send({ message: "Invalid Link" });
  }
  User.findOne({
    where: { email: email }
  })
    .then(function(User) {
      if (User) {
        if (
          QuickEncrypt.decrypt(req.query.id, privateKey) === User.verifyUserSalt
        ) {
          User.update({ verified: true, verifyUserSalt: null }).then(
            function() {
              return res.status(200).send({ message: "User Verified" });
            }
          );
        } else {
          return res.status(400).send({ message: "Bad request" });
        }
      } else {
        return res.status(401).send({ message: "User not found" });
      }
    })
    .catch(err => {
      return res.status(400).send({ message: "Link expired" });
    });
};

//checking before email verification
exports.isAlreadyVerified = (req, res, next) => {
  User.findOne({ where: { email: req.user.email } }).then(User => {
    if (User.dataValues.verified === true) {
      return res.status(200).send({ message: "User Verified" });
    } else {
      next();
    }
  });
};

//checking verified users before allowing to bid
exports.checkVerifyUser = (req, res, next) => {
  User.findOne({ where: { email: req.user.email } }).then(User => {
    if (User.dataValues.verified === true) {
      next();
    } else {
      return res.status(401).send({ message: "User not verified" });
    }
  });
};
