const dotenv = require("dotenv");
dotenv.config();

exports.environment = [
  {
    host: process.env.DEV_ENVIRONMENT,
    url: process.env.base_local_url,
    swag: process.env.swag_local
  },
  {
    host: process.env.PROD_ENVIRONMENT,
    url: process.env.base_url,
    swag: process.env.swag_dev
  }
];
