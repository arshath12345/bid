const crypto = require("crypto");
const uuidv1 = require("uuid/v1");
const passport = require("passport");
const jwt = require("jsonwebtoken");

const User = require("../models").user;
const auth = require("../config/passport-setup");

auth(passport);

exports.google = passport.authenticate("google", {
  scope: ["profile", "email"]
});

exports.redirect = (req, res, next) => {
  if (!req.user) {
    return res.send("Hi");
  }
  const token = jwt.sign(
    { name: req.user.dataValues.username, email: req.user.dataValues.email },
    process.env.JWT_ENCRYPTION,
    {
      expiresIn: "1h"
    }
  );
  return res.status(200).send({
    message: "Login Successful",
    token: token
  });
};
