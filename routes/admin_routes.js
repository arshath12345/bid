const express = require("express");
const router = express.Router();

const Admin = require("../controllers/admin");
const auth = require("../controllers/auth");
const Validator = require("../validators/index");

router.post("/adminLogin", Validator.adminSigninValidator, Admin.login);
router.post("/adminRegister", Validator.adminRegisterValidator, Admin.register);
router.post(
  "/addProducts",
  Validator.addProductsValidator,
  auth.checkAuthAdmin,
  Admin.addProducts
);
router.post(
  "/updateProduct",
  Validator.updateProductsValidator,
  auth.checkAuthAdmin,
  Admin.updateProduct
);
router.post(
  "/deleteProduct",
  Validator.deleteProductsValidator,
  auth.checkAuthAdmin,
  Admin.deleteProduct
);
router.get("/listAllProducts", auth.checkAuthAdmin, Admin.listAllProducts);

module.exports = router;
