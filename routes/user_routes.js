const express = require("express");
const router = express.Router();

const User = require("../controllers/user");
const auth = require("../controllers/auth");
const send = require("../controllers/sendgrid");
const Validator = require("../validators/index");

//user registration
router.post("/userRegister", Validator.userRegisterValidator, User.register);

//opting 2-factor authentication
router.post(
  "/getTwoFactorAuth",
  auth.checkAuthUser,
  auth.checkVerifyUser,
  User.getTwoFactor
);

//remove 2-factor authentication
router.post(
  "/removeTwoFactorAuth",
  auth.checkAuthUser,
  auth.checkVerifyUser,
  User.removeTwoFactor
);

//user login - 2FA included
router.post(
  "/userLogin",
  Validator.userLoginValidator,
  User.login,
  auth.generateOTPSendMail,
  send.sendMail
);
router.get(
  "/userLogin2FAPage",
  Validator.verifyTwoFactorLinkValidator,
  auth.verifyTwoFactorLink
);
router.post(
  "/userLogin2FA",
  Validator.verifyTwoFactorValidator,
  auth.verifyTwoFactor,
  User.twofactorLogin
);

//verify user
router.post(
  "/verifyUser",
  auth.checkAuthUser,
  auth.isAlreadyVerified,
  auth.verifyUser,
  send.sendMail
);
router.get("/verifyUserLink", auth.verifyUserLink);

//forget and reset password
router.post(
  "/forgotPassword",
  Validator.forgotPasswordValidator,
  auth.forgotPassword,
  send.sendMail
);
router.get(
  "/fogotPwdLink",
  Validator.forgotPasswordLinkValidator,
  auth.forgotPasswordLink
);
router.post("/resetPwd", Validator.resetPasswordValidator, auth.resetPwd);

//bidding routes..
router.get(
  "/getActiveProducts",
  auth.checkAuthUser,
  auth.checkVerifyUser,
  User.getActiveProducts
);
router.post(
  "/bid",
  Validator.bidValidator,
  auth.checkAuthUser,
  auth.checkVerifyUser,
  User.bid
);

//after bidding routes.
router.get(
  "/boughtProducts",
  auth.checkAuthUser,
  auth.checkVerifyUser,
  User.boughtProducts
);

//checking history of biddings
router.get(
  "/getHistory",
  auth.checkAuthUser,
  auth.checkVerifyUser,
  User.getHistory
);

module.exports = router;
