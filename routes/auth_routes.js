const express = require("express");
const passport = require("passport");

const router = express.Router();

const Auth = require("../controllers/google_routes");

router.get("/google", Auth.google);
router.get("/google/redirect", passport.authenticate("google"), Auth.redirect);

module.exports = router;
