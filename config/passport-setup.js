const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const User = require("../models").user;
const crypto = require("crypto");
const uuidv1 = require("uuid/v1");
const jwt = require("jsonwebtoken");
const dotenv = require("dotenv");
dotenv.config();

const environment = require("../controllers/environment_chooser").environment;

module.exports = passport => {
  passport.use(
    new GoogleStrategy(
      {
        clientID: process.env.clientID,
        clientSecret: process.env.clientSecret,
        callbackURL: environment[process.env.ENVI].url + "/auth/google/redirect"
      },
      (accessToken, refreshToken, profile, done) => {
        const g_username = profile._json.name;
        const g_email = profile._json.email;
        const g_verified = profile._json.email_verified;
        const g_password = null;
        new_salt = uuidv1();
        User.findOne({
          where: { email: g_email }
        }).then(function(check_user) {
          if (check_user) {
            return done(null, check_user);
          } else {
            User.create({
              username: g_username,
              email: g_email,
              password: g_password,
              verified: g_verified,
              salt: new_salt
            }).then(function(user) {
              if (user) {
                return done(null, user);
              } else {
                return res.status(400).send({
                  message: "Server error"
                });
              }
            });
          }
        });
      }
    )
  );
  passport.serializeUser((user, done) => {
    done(null, user);
  });

  passport.deserializeUser((user, done) => {
    done(null, user);
  });
};
