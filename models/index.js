"use strict";
const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const basename = path.basename(__filename);
const dotenv = require("dotenv");
dotenv.config();

const environment = require("../controllers/environment_chooser").environment;
const config = require(__dirname + process.env.CONFIG_DIR)[
  environment[process.env.ENVI].host
];

const db = {};

const client_key = fs.readFileSync(
  __dirname + process.env.CLIENT_KEY_DIR,
  "utf-8"
);

const server_ca = fs.readFileSync(__dirname + process.env.SERVER_KEY, "utf-8");

const client_cert = fs.readFileSync(
  __dirname + process.env.CLIENT_CERT,
  "utf-8"
);

let sequelize;
if (environment[process.env.ENVI].host === "production") {
  sequelize = new Sequelize(
    process.env.DATABASE,
    process.env.DB_USERNAME,
    process.env.DB_PASSWORD,
    {
      host: process.env.DB_HOST,
      dialect: process.env.DIALECT,
      socketPath: process.env.SOCKET_PATH,
      dialectOptions: {
        ssl: {
          key: client_key,
          cert: client_cert,
          ca: server_ca
        }
      },
      port: process.env.REMOTE_DB_PORT
    }
  );
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, {
    host: config.host,
    dialect: process.env.DIALECT,
    port: process.env.LOCAL_DB_PORT
  });
}
fs.readdirSync(__dirname)
  .filter(file => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach(file => {
    const model = sequelize["import"](path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
    sequelize.sync({ force: false }).then(() => {
      console.log(`Database & tables are created`);
    });
  })
  .catch(err => {
    console.error("Unable to connect to the database:", err);
  });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
