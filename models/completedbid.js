"use strict";
module.exports = (sequelize, DataTypes) => {
  const CompletedBid = sequelize.define(
    "CompletedBid",
    {
      product_name: DataTypes.STRING,
      initial_bid: DataTypes.FLOAT,
      final_bid: DataTypes.FLOAT,
      buyer: DataTypes.STRING,
      active: DataTypes.BOOLEAN
    },
    {}
  );
  CompletedBid.associate = function(models) {
    // associations can be defined here
  };
  return CompletedBid;
};
