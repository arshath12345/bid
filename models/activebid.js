"use strict";
module.exports = (sequelize, DataTypes) => {
  const ActiveBid = sequelize.define(
    "ActiveBid",
    {
      product_name: DataTypes.STRING,
      initial_bid: DataTypes.FLOAT,
      current_bid: DataTypes.FLOAT,
      buyer: DataTypes.STRING,
      active: DataTypes.BOOLEAN
    },
    {}
  );
  ActiveBid.associate = function(models) {
    // associations can be defined here
  };
  return ActiveBid;
};
