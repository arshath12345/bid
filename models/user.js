"use strict";
module.exports = (sequelize, DataTypes) => {
  const user = sequelize.define(
    "user",
    {
      username: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      salt: DataTypes.STRING,
      forgotPasswordSalt: DataTypes.STRING,
      verifyUserSalt: DataTypes.STRING,
      verified: DataTypes.BOOLEAN,
      two_factor: DataTypes.BOOLEAN,
      two_factor_salt: DataTypes.STRING,
      OTP: DataTypes.STRING
    },
    {}
  );
  user.associate = function(models) {
    // associations can be defined here
  };
  return user;
};
