"use strict";
module.exports = (sequelize, DataTypes) => {
  const History = sequelize.define(
    "History",
    {
      product_name: DataTypes.STRING,
      prev_user1: DataTypes.STRING,
      prev_amount1: DataTypes.FLOAT,
      prev_user2: DataTypes.STRING,
      prev_amount2: DataTypes.FLOAT,
      prev_user3: DataTypes.STRING,
      prev_amount3: DataTypes.FLOAT,
      prev_user4: DataTypes.STRING,
      prev_amount4: DataTypes.FLOAT,
      prev_user5: DataTypes.STRING,
      prev_amount5: DataTypes.FLOAT
    },
    {}
  );
  History.associate = function(models) {
    // associations can be defined here
  };
  return History;
};
