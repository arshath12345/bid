const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const swaggerUi = require("swagger-ui-express");
const swaggerDocument = require("./swagger.json");
const passport = require("passport");
const adminRoutes = require("./routes/admin_routes.js");
const userRoutes = require("./routes/user_routes.js");
const authRoutes = require("./routes/auth_routes.js");
const expressValidator = require("express-validator");
const environment = require("./controllers/environment_chooser").environment;

//app
const app = express();

//port
const port = process.env.PORT;

//body parser
app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));

//initialize passport
app.use(passport.initialize());
app.use(passport.session());

//for security reasons.
var helmet = require("helmet");
app.use(helmet());

//Environment variables
const dotenv = require("dotenv");
dotenv.config();

//finding the environment and setting the link for documentation
swaggerDocument.host = environment[process.env.ENVI].swag;

//validator
app.use(expressValidator());

//admin routes
app.use("/admin", adminRoutes);

//user routes
app.use("/user", userRoutes);

//google-signin
app.use("/auth", authRoutes);

//documentation
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get("/", (req, res, next) => {
  res.status(200).send({ message: "Success" });
});

app.listen(port, () => {
  console.log(`Server started`);
});
