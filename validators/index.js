exports.adminRegisterValidator = (req, res, next) => {
  req.check("username", "Username is required").notEmpty();
  req.check("password", "Password is required").notEmpty();
  req
    .check("password")
    .isLength({
      min: 6
    })
    .withMessage("Password must contain atleast six characters")
    .matches(/\d/)
    .withMessage("Password must contain a number");
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.adminSigninValidator = (req, res, next) => {
  req.check("username", "Username is required").notEmpty();
  req.check("password", "Password is required").notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.addProductsValidator = (req, res, next) => {
  req.check("products.*.product_name", "Product Name is required").notEmpty();
  req.check("products.*.initial_bid", "Initial Bid is required").notEmpty();
  req
    .check("products.*.initial_bid")
    .matches(/[0-9]+/)
    .withMessage("Initial Bid should be a number");
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.deleteProductsValidator = (req, res, next) => {
  req.check("product_name", "Product Name is required").notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.updateProductsValidator = (req, res, next) => {
  req.check("product_name", "Product Name is required").notEmpty();
  req.check("initial_bid", "Initial Bid is required").notEmpty();
  req
    .check("initial_bid")
    .matches(/[0-9]+/)
    .withMessage("Initial Bid should be a number");
  req.check("active", "Active field is required").isBoolean();
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.userRegisterValidator = (req, res, next) => {
  req.check("username", "Username is required").notEmpty();
  req.check("email", "Email is required").notEmpty();
  req
    .check("email", "Email must be between 3 and 32 chars")
    .matches(/.+\@.+\..+/)
    .withMessage("Invalid email")
    .isLength({
      min: 3,
      max: 31
    });
  req.check("password", "Password is required").notEmpty();
  req
    .check("password")
    .isLength({ min: 6 })
    .withMessage("Password must contain atleast six characters")
    .matches(/\d/)
    .withMessage("Password must contain a number");
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.userLoginValidator = (req, res, next) => {
  req
    .check("email", "Email must be between 3 and 32 chars")
    .matches(/.+\@.+\..+/)
    .withMessage("Invalid email")
    .isLength({
      min: 3,
      max: 31
    });
  req.check("password", "Password is required").notEmpty();
  req
    .check("password")
    .isLength({ min: 6 })
    .withMessage("Password must contain atleast six characters")
    .matches(/\d/)
    .withMessage("Password must contain a number");
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.forgotPasswordValidator = (req, res, next) => {
  req
    .check("email", "Email must be between 3 nd 32 chars")
    .matches(/.+\@.+\..+/)
    .withMessage("Invalid email")
    .isLength({
      min: 3,
      max: 31
    });
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.bidValidator = (req, res, next) => {
  req.check("product_name", "Product Name is required").notEmpty();
  req.check("amount", "Amount is required").notEmpty();
  req
    .check("amount")
    .matches(/[0-9]+/)
    .withMessage("Amount should be a number");
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.verifyTwoFactorValidator = (req, res, next) => {
  req.check("otp", "OTP is required").notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({
      error: firstError
    });
  }
  next();
};

exports.verifyTwoFactorLinkValidator = (req, res, next) => {
  req.check("u", "Invalid Link").notEmpty();
  req.check("id", "Invalid Link").notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.forgotPasswordLinkValidator = (req, res, next) => {
  req.check("u", "Invalid Link").notEmpty();
  req.check("id", "Invalid Link").notEmpty();
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({ error: firstError });
  }
  next();
};

exports.resetPasswordValidator = (req, res, next) => {
  req.check("pwd", "Password is required").notEmpty();
  req
    .check("pwd")
    .isLength({
      min: 6
    })
    .withMessage("Password must contain atleast six characters")
    .matches(/\d/)
    .withMessage("Password must contain a number");
  const errors = req.validationErrors();
  if (errors) {
    const firstError = errors.map(error => error.msg)[0];
    return res.status(400).json({
      error: firstError
    });
  }
  next();
};
