"use strict";
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable("Histories", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      product_name: {
        type: Sequelize.STRING
      },
      prev_user1: {
        type: Sequelize.STRING
      },
      prev_amount1: {
        type: Sequelize.FLOAT
      },
      prev_user2: {
        type: Sequelize.STRING
      },
      prev_amount2: {
        type: Sequelize.FLOAT
      },
      prev_user3: {
        type: Sequelize.STRING
      },
      prev_amount3: {
        type: Sequelize.FLOAT
      },
      prev_user4: {
        type: Sequelize.STRING
      },
      prev_amount4: {
        type: Sequelize.FLOAT
      },
      prev_user5: {
        type: Sequelize.STRING
      },
      prev_amount5: {
        type: Sequelize.FLOAT
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable("Histories");
  }
};
